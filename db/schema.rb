# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150924214358) do

  create_table "empresas", force: :cascade do |t|
    t.string   "nombre"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "empresas", ["email"], name: "index_empresas_on_email", unique: true

  create_table "envioempresas", force: :cascade do |t|
    t.string   "e_calle_re"
    t.string   "e_num_ext_re"
    t.string   "e_num_int_re"
    t.string   "e_colonia_re"
    t.string   "e_delegacion_re"
    t.string   "e_cp_re"
    t.string   "e_remitente"
    t.string   "e_calle_de"
    t.string   "e_num_ext_de"
    t.string   "e_num_int_de"
    t.string   "e_colonia_de"
    t.string   "e_delegacion_de"
    t.string   "e_cp_de"
    t.string   "e_destinatario"
    t.datetime "fecha"
    t.string   "hora"
    t.string   "telefono"
    t.string   "orden"
    t.integer  "empresa_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "notas"
  end

  add_index "envioempresas", ["empresa_id", "created_at"], name: "index_envioempresas_on_empresa_id_and_created_at"
  add_index "envioempresas", ["empresa_id"], name: "index_envioempresas_on_empresa_id"

  create_table "microenvios", force: :cascade do |t|
    t.text     "pickup"
    t.text     "dropoff"
    t.integer  "precio"
    t.string   "mpago"
    t.integer  "telefono"
    t.text     "notas"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "microenvios", ["usuario_id", "created_at"], name: "index_microenvios_on_usuario_id_and_created_at"
  add_index "microenvios", ["usuario_id"], name: "index_microenvios_on_usuario_id"

  create_table "microposts", force: :cascade do |t|
    t.string   "callere"
    t.string   "num_ext_re"
    t.string   "num_int_re"
    t.string   "colonia_re"
    t.string   "delegacion_re"
    t.string   "cp_re"
    t.string   "remitente"
    t.string   "calle_de"
    t.string   "num_ext_de"
    t.string   "num_int_de"
    t.string   "colonia_de"
    t.string   "delegacion_de"
    t.string   "cp_de"
    t.string   "destinatario"
    t.integer  "precio"
    t.text     "notas"
    t.string   "telefono"
    t.integer  "usuario_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "mpago"
    t.string   "num_orden"
  end

  add_index "microposts", ["usuario_id", "created_at"], name: "index_microposts_on_usuario_id_and_created_at"
  add_index "microposts", ["usuario_id"], name: "index_microposts_on_usuario_id"

  create_table "ordens", force: :cascade do |t|
    t.text     "pickup"
    t.text     "dropoff"
    t.integer  "distancia"
    t.string   "precio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "nombre"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true

end
