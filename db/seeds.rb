# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Usuario.create!(nombre:  "Administrador",
             email: "bicimensajero420@gmail.com",
             password:              "Biclas420!",
             password_confirmation: "Biclas420!",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)