class CreateMicroenvios < ActiveRecord::Migration
  def change
    create_table :microenvios do |t|
      t.text :pickup
      t.text :dropoff
      t.integer :precio
      t.string :mpago
      t.integer :telefono
      t.text :notas
      t.references :usuario, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :microenvios, [:usuario_id, :created_at]
  end
end
