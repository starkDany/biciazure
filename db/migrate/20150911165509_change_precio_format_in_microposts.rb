class ChangePrecioFormatInMicroposts < ActiveRecord::Migration
  def up
    change_column :microposts, :precio, 'integer USING CAST(precio AS integer)'
  end

  def down
    change_column :microposts, :precio, :string
  end
end
