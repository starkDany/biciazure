class CreateEnvioempresas < ActiveRecord::Migration
  def change
    create_table :envioempresas do |t|
      t.string :e_calle_re
      t.string :e_num_ext_re
      t.string :e_num_int_re
      t.string :e_colonia_re
      t.string :e_delegacion_re
      t.string :e_cp_re
      t.string :e_remitente
      t.string :e_calle_de
      t.string :e_num_ext_de
      t.string :e_num_int_de
      t.string :e_colonia_de
      t.string :e_delegacion_de
      t.string :e_cp_de
      t.string :e_destinatario
      t.datetime :fecha
      t.string :hora
      t.string :telefono
      t.string :orden
      t.references :empresa, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :envioempresas, [:empresa_id, :created_at]
  end
end
