class AddResetToEmpresas < ActiveRecord::Migration
  def change
    add_column :empresas, :reset_digest, :string
    add_column :empresas, :reset_sent_at, :datetime
  end
end
