class AddIndexToEmpresasEmail < ActiveRecord::Migration
  def change
  	add_index :empresas, :email, unique: true
  end
end
