class AddPasswordDigestToEmpresas < ActiveRecord::Migration
  def change
    add_column :empresas, :password_digest, :string
  end
end
