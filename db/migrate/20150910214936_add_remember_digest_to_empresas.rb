class AddRememberDigestToEmpresas < ActiveRecord::Migration
  def change
    add_column :empresas, :remember_digest, :string
  end
end
