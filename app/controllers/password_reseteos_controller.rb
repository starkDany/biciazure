class PasswordReseteosController < ApplicationController
	before_action :get_empresa,   only: [:edit, :update]
	before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
  	@empresa = Empresa.find_by(email: params[:password_reseteo][:email].downcase)
  	if @empresa
  		@empresa.create_reset_digest
  		@empresa.send_password_reseteo_email
  		flash[:info] = "Mail enviado con instrucciones"
  		redirect_to root_url
  	else
  		flash[:danger] = "No se encontró el email en nuestra base"
  		render 'new'
  	end
  end

  def edit
  end

  def update
  	if params[:empresa][:password].empty?
  		@empresa.errors.add(:password, "No puede estar vacio")
  		render 'edit'
  	elsif @empresa.update_attributes(empresa_params)
  		ingresa @empresa
  		flash[:success] = "Se recuperó el password correctamente"
  		redirect_to @empresa
  	else
  		render 'edit'
  	end
  end

  private

  def empresa_params
  	params.require(:empresa).permit(:password, :password_confirmation)
  end

  def get_empresa
  	@empresa = Empresa.find_by(email: params[:email])
  end

  def check_expiration
  	if @empresa.password_reseteo_expired?
  		flash[:danger] = "Expiró el link"
  		redirect_to new_password_reseteo_url
  	end
  end

end
