class MicropostsController < ApplicationController
  before_action :logged_in_usuario, only: [:create, :destroy]

  def create
    @micropost = current_usuario.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Bicimensajero en camino!"
      redirect_to current_usuario
    else
      @feed_items = []
      flash.now[:warning] = "No se pudo completar la solicitud :("
      render 'estaticas/home'
    end
  end

  def destroy
  end

  private

    def micropost_params
      params.require(:micropost).permit(:callere, :num_ext_re, :num_int_re, :colonia_re, :delegacion_re, :cp_re, :remitente, :calle_de, :num_ext_de, :num_int_de, :colonia_de, :delegacion_de, :cp_de, :destinatario, :precio, :notas, :telefono, :mpago, :num_orden)
    end
end
