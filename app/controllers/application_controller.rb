class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  include EmpresessionsHelper
  
  private

  def logged_in_usuario
      unless logged_in?
        store_location
        flash.now[:danger] = "Por favor ingresa a tu cuenta"
        redirect_to login_url
      end
    end

    #Confirmación de empresa loggeada
  def logged_in_empresa
    unless ingresado?
      store_location
      flash[:danger] = "Por favor ingresa a tu cuenta"
      redirect_to ingresa_url
    end
  end

      # Confirms an admin user.
  def admin_usuario
    redirect_to(root_url) unless current_usuario.admin?
  end

end
