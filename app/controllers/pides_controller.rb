class PidesController < ApplicationController
  def new
    @pide = Pide.new
  end

  def create
    @pide = Pide.new(params[:pide])
    @pide.request = request
    if @pide.deliver
      flash[:success] = 'Tu bicimensajero está confirmado, te enviamos un mail con los datos.'
      redirect_to root_url
    else
      flash.now[:warning] = 'No se pudo enviar tu cotizacion, por favor revisa todos los campos a llenar'
      render :new
    end
  end
end
