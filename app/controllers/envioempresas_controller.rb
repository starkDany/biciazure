class EnvioempresasController < ApplicationController
	before_action :logged_in_empresa, only: [:create, :destroy]

	def create
		@envioempresa = current_empresa.envioempresas.build(envioempresa_params)
		if @envioempresa.save
			flash[:success] = "Envío agendado"
			redirect_to root_url
		else
			render 'estaticas/home'
		end
	end

	def destroy
	end

	def envioempresa_params
		params.require(:envioempresa).permit(:e_calle_re, :e_num_ext_re, :e_num_int_re, :e_colonia_re, :e_delegacion_re, :e_cp_re, :e_remitente, :e_calle_de, :e_num_ext_de, :e_num_int_de, :e_colonia_de, :e_delegacion_de, :e_cp_de, :e_destinatario, :fecha, :hora, :telefono, :orden, :notas)
	end

end
