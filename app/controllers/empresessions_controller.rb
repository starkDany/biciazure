class EmpresessionsController < ApplicationController

  def new
  end

  def create
  	empresa = Empresa.find_by(email: params[:session][:email].downcase)
  	if empresa && empresa.authenticate(params[:session][:password])
  		ingresa empresa
  		params[:session][:remember_me] == '1' ? remember(empresa) : forget(empresa)
  		redirect_back_or empresa
  	else
  		flash.now[:danger] = 'Mail o contraseña inválido'
  		render 'new'
  	end
  end

  def destroy
  	cerrar_sesion if ingresado?
  	redirect_to root_url
  end

end
