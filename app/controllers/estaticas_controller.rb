class EstaticasController < ApplicationController

  def home

    if logged_in?
      @micropost  = current_usuario.microposts.build
      @feed_items = current_usuario.feed.paginate(page: params[:page])
    else ingresado?
      @envioempresa = current_empresa.envioempresas.build if ingresado?
    end

  end

  def cotiza
  end

  def acercade
  end

  def faq
  end

  def unete
  end

  def politicas
  end

  def contacto
  end

  def privacidad
  end

  def rutas
  end

end
