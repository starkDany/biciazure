class SessionsController < ApplicationController

  def new
  end

  def create
    usuario = Usuario.find_by(email: params[:session][:email].downcase)
    if usuario && usuario.authenticate(params[:session][:password])
      if usuario.activated?
        log_in usuario
        params[:session][:remember_me] == '1' ? remember(usuario) : forget(usuario)
        redirect_back_or usuario
        remember usuario
    else
      message  = "Tu cuenta no está activada. "
      message += "Checa tu email por el link de activación."
      flash[:warning] = message
      redirect_to root_url
    end
  else
      flash.now[:danger] = 'Mail o password incorrectos, por favor trata de nuevo.'
      render 'new'
    end
  end

  def destroy
  	log_out if logged_in?
    redirect_to root_url
  end
end
