class EmpresasController < ApplicationController
  before_action :logged_in_empresa, only: [:edit, :update]
  before_action :correct_empresa,   only: [:edit, :update]
  before_action :admin_usuario,     only: [:index, :destroy]

  def index
    @empresas = Empresa.paginate(page: params[:page], :per_page => 20)
  end

  def show
  	@empresa = Empresa.find(params[:id])
    @envioempresas = @empresa.envioempresas.paginate(page: params[:page])
  end

  def nueva
  	@empresa = Empresa.new
  end

  def create
  	@empresa = Empresa.new(empresa_params)
  	if @empresa.save
      ingresa @empresa
  		flash.now[:success] = "Empresa dada de alta correctamente"
  		redirect_to @empresa
  	else
  		render 'nueva'
  	end
  end

  def edit
    @empresa = Empresa.find(params[:id])
  end

  def update
    @empresa = Empresa.find(params[:id])
    if @empresa.update_attributes(empresa_params)
      flash[:success] = "¡Perfil actualizado!"
      redirect_to @empresa
    else
      render 'edit'
    end
  end

  def destroy
    Empresa.find(params[:id]).destroy
    flash.now[:success] = "Empresa borrada"
    redirect_to empresas_url
  end

  private

  def empresa_params
  	params.require(:empresa).permit(:nombre, :email, :password, :password_confirmation)
  end

  #BEFORE filters

  def correct_empresa
    @empresa = Empresa.find(params[:id])
    redirect_to(root_url) unless current_empresa?(@empresa)
  end

end
