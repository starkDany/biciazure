module EmpresessionsHelper

	def ingresa(empresa)
		session[:empresa_id] = empresa.id
	end

	def remember(empresa)
		empresa.remember
		cookies.permanent.signed[:empresa_id] = empresa.id
		cookies.permanent[:remember_token] = empresa.remember_token
	end

	def current_empresa?(empresa)
    	empresa == current_empresa
  	end

	def current_empresa
	    if (empresa_id = session[:empresa_id])
	      @current_empresa ||= Empresa.find_by(id: empresa_id)
	    elsif (empresa_id = cookies.signed[:empresa_id])
	      empresa = Empresa.find_by(id: empresa_id)
	      if empresa && empresa.authenticated?(:remember, cookies[:remember_token])
	        ingresa empresa
	        @current_empresa = empresa
	      end
	    end
	end

	def ingresado?
		!current_empresa.nil?
	end

	def cerrar_sesion
		forget(current_empresa)
		session.delete(:empresa_id)
		@current_empresa = nil
	end

	def redirect_back_or(default)
		redirect_to(session[:forwarding_url] || default)
		session.delete(:forwarding_url)
	end

	def store_location
		session[:forwarding_url] = request.url if request.get?
	end

end
