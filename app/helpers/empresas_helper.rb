module EmpresasHelper
	# Returns the Gravatar for the given user.
  def gravatar_for(empresa)
    gravatar_id = Digest::MD5::hexdigest(empresa.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: empresa.nombre, class: "gravatar")
  end
end
