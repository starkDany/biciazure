class Empresa < ActiveRecord::Base
  has_many :envioempresas, dependent: :destroy
  attr_accessor :remember_token, :reset_token
  before_save { self.email = email.downcase }
  validates :nombre, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  def Empresa.digest(string)
  	    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  	  BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
  end

  def Empresa.new_token
  	SecureRandom.urlsafe_base64
  end

  def remember
  	self.remember_token = Empresa.new_token
    update_attribute(:remember_digest, Empresa.digest(remember_token))
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
  	return false if remember_digest.nil?
  	BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
  	update_attribute(:remember_digest, nil)
  end

  def create_reset_digest
    self.reset_token = Empresa.new_token
    update_attribute(:reset_digest,  Empresa.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reseteo_email
    UsuarioMailer.password_reseteo(self).deliver_now
  end

  def password_reseteo_expired?
    reset_sent_at < 2.hours.ago
  end

end
