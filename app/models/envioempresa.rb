class Envioempresa < ActiveRecord::Base
  belongs_to :empresa
  after_create :enviar_email
  default_scope -> { order(created_at: :desc) }
  validates :empresa_id, presence: true
  validates :e_calle_re, presence: true
  validates :e_num_ext_re, presence: true
  validates :e_colonia_re, presence: true
  validates :e_delegacion_re, presence: true
  validates :e_cp_re, presence: true
  validates :e_remitente, presence: true
  validates :e_calle_de, presence: true
  validates :e_num_ext_de, presence: true
  validates :e_colonia_de, presence: true
  validates :e_delegacion_de, presence: true
  validates :e_cp_de, presence: true
  validates :e_destinatario, presence: true
  validates :fecha, presence: true
  validates :hora, presence: true
  validates :telefono, presence: true
  validates :orden, presence: true
  validates_date :fecha, :after => lambda { Date.current.advance(:days => -2) }, :after_message => "La fecha no puede ser anterior a hoy"
  validates_time :hora, :between => '8:30am'...'6:30pm', :before_message => "Nuestro horario es de 8:30am a 7:00pm"
end

def enviar_email
  Bicimensajeros.envia_mail( self.empresa, self ).deliver
  Bicimensajeros.envia_mailP( self.empresa, self ).deliver
end