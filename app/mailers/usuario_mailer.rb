class UsuarioMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usuario_mailer.account_activation.subject
  #
  def account_activation(usuario)
    @usuario = usuario
    mail to: usuario.email, from: "noresponderaqui@bicimensajero.com", subject: "Activa tu cuenta en bicimensajero"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usuario_mailer.password_reset.subject
  #
  def password_reset(usuario)
    @usuario = usuario
    mail to: usuario.email, from: "noresponderaqui@bicimensajero.com", subject: "Recupera tu password de bicimensajero"
  end

  def password_reseteo(empresa)
    @empresa = empresa
    mail to: empresa.email, subject: "Recupera tu password de bicimensajero", from: "noresponderaqui@bicimensajero.com"
  end

end
