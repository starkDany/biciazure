class ApplicationMailer < ActionMailer::Base
  default from: "noresponderaqui@bicimensajero.com"
  layout 'mailer'
end
