class Bicimensajeros < ApplicationMailer

	def post_email( usuario, micropost )
	@usuario = usuario
	@micropost = micropost

    mail(:from => "noresponderaqui@bicimensajero.com", :to => "#{usuario.nombre} <#{usuario.email}>", :subject => "Bicimensajero - Orden #{micropost.num_orden}")

   end

	def post_emailP( usuario, micropost )
	@usuario = usuario
	@micropost = micropost

    mail(:from => "noresponderaqui@bicimensajero.com", :to => "amo@bicimensajero.com", :bcc => "danielvp1987@gmail.com", :subject => "Nuevo pedido - Orden #{micropost.num_orden}")

   end

   def envia_mail( empresa, envioempresa )
   	@empresa = empresa
	@envioempresa = envioempresa
	mail(:from => "noresponderaqui@bicimensajero.com", :to => "#{empresa.nombre} <#{empresa.email}>", :subject => "Bicimensajero - Orden #{envioempresa.orden}")
   end

   def envia_mailP( empresa, envioempresa )
   	@empresa = empresa
	@envioempresa = envioempresa
	mail(:from => "noresponderaqui@bicimensajero.com", :to => "amo@bicimensajero.com", :bcc => "danielvp1987@gmail.com", :subject => "Bicimensajero - Orden #{envioempresa.orden}")
   end

end
